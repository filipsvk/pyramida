class Pyramida {
    public static void main(String[] args) {
        // definuj n pre vysku pyramidy
        int n = 4;
        int o = n;
        int i = 1;
        int m = 0;
        int k = 1;
        for (int u = 0; u < o; u++) {
            while (i < n) {
                System.out.print(" ");
                i++;
            }
            i = 1;
            while (m < k) {
                System.out.print("*");
                m++;
            }
            m = 0;
            System.out.println(" ");
            k = k + 2;
            n = n - 1;
        }

    }
}